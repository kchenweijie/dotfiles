# ~/.bash_aliases
alias reset='reset && . ~/.bashrc'
alias gccw='gcc -W -Wall'
# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
alias svim='sudo vim'

alias lsd='ls -d */'
alias lad='ls -d */ .*/'

alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -i'

alias dblogin='mysql -uroot -ppassword'
alias vactivate='source ./.venv/bin/activate --no-site-packages'
alias gotoquantum='cd ~/openstack/quantum/quantum'
alias gotoquark='cd ~/openstack/quantum/quark/quark'
