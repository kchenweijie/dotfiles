set nocompatible
" Setting up views
let g:skipview_files = ['py']
source ~/.vim/restore_view.vim

" Setting columns and tabbing
set ts=4
set shiftwidth=4
set softtabstop=4
set expandtab
set nu
setlocal foldcolumn=1

" Filetype and syntax highlighting.
syntax on
filetype off
filetype plugin indent on
setlocal fdm=manual

" Remaps
let mapleader = ','
nnoremap <silent> <Space> @=(foldlevel('.')?'za':"\<Space>")<CR>
vnoremap <Space> zf
nnoremap <up> gk
nnoremap <down> gj

" Persistent undo
set undodir=~/.vim/undo
set undofile
set undolevels=1000 " Max changes
set undoreload=10000 " Max number to save on buffer reload

" Set up plugins
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()
Bundle 'gmarik/vundle'

Bundle 'scrooloose/nerdcommenter'
Bundle 'xolox/vim-misc'
Bundle 'xolox/vim-easytags'
Bundle 'flazz/vim-colorschemes'
Bundle 'rkulla/pydiction'
Bundle 'tpope/vim-fugitive'
Bundle 'andviro/flake8-vim'

set tags=./.tags;
let g:easytags_dynamic_files = 1
let g:easytags_events = ['BufWritePost']
let g:pydiction_location = '/home/kevin/.vim/bundle/pydiction/complete-dict'
let g:PyFlakeCheckers = 'pep8,pyflakes'

color mustang
hi Folded ctermbg=59
hi ColorColumn ctermbg=60
autocmd FileType python source /home/kevin/.vim/jpythonfold.vim | set colorcolumn=80

autocmd FileType python source ~/.vim/jpythonfold.vim

color chela_light
" Auto refresh .vimrc upon change.
autocmd BufWritePost /home/kevin/.vimrc so /home/kevin/.vimrc
